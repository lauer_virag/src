package org.lauer.BmiIndex;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestBmi {

	private Main tester;

	@Before
	public void setUp() throws Exception {
		tester = new Main();
	}

	@Test
	public void test() {
		Main test = new Main();
		final double DELTA = 1e-15;
		double result = test.bmiindexszamitas(60, 1.8);
		assertEquals(18.51851851851852, result, DELTA);

	}
	/*
	 * @Test public void test() { Main test = new Main(); // int output =
	 * test.bmi(18.51851851851852); // assertEquals(1.8, 60, output);
	 * 
	 * }
	 */

}
