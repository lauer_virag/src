package org.lauer.BmiIndex;

public class Data {

	private double height;
	private double weight;
	private double age;
	private double bmi;

	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	public Data(double height, double weight, double age, double bmi) {
		this.height = height;
		this.weight = weight;
		this.age = age;
		this.bmi = bmi;
	}

	public double bmiindexszamitas(double height, double weight) {
		this.height = height;
		this.weight = weight;
		this.bmi = weight / (height * height);
		return bmi;
	}

}
