package org.lauer.BmiIndex;

public class Main {

	double bmi;

	public double bmiindexszamitas(double height, double weight) {
		bmi = weight / (height * height);
		return bmi;
	}

	public static void main(String[] args) {

		Data ember = new Data(1.8, 60, 25, 0);

		ember.setBmi(ember.getWeight() / (ember.getHeight() * ember.getHeight()));

		System.out.println(ember.getBmi());

		ember.setBmi(ember.bmiindexszamitas(1.8, 60));
		System.out.println(ember.getBmi());

		if (ember.getBmi() < 16)
			System.out.println(" Severe Thinness");
		else if (ember.getBmi() >= 16 || ember.getBmi() < 17)
			System.out.println("Moderate Thinness");
		else if (ember.getBmi() >= 17 || ember.getBmi() < 18.5)
			System.out.println("Mild Thinness");
		else if (ember.getBmi() >= 18.5 || ember.getBmi() < 25)
			System.out.println("Normal");
		else if (ember.getBmi() >= 25 || ember.getBmi() < 30)
			System.out.println("Overweight");
		else if (ember.getBmi() >= 30 || ember.getBmi() < 35)
			System.out.println("Obese Class I");
		else if (ember.getBmi() >= 35 || ember.getBmi() < 40)
			System.out.println("Obese Class II");
		else if (ember.getBmi() >= 40)
			System.out.println("Obese Class III");

	}

}
